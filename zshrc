# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH
if [ -d "$HOME/.dotfiles" ]; then 
  export DOTFILES="$HOME/.dotfiles"
elif [ -d "/usr/local/dotfiles" ]; then
  export DOTFILES="/usr/local/dotfiles"
else 
  echo "No dotfiles installation found."
fi

# Path to your oh-my-zsh installation.
export ZSH="$DOTFILES/oh-my-zsh"

# @see https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
ZSH_THEME="agnoster"

# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to automatically update without prompting.
# DISABLE_UPDATE_PROMPT="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS=true

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="yyyy-mm-dd"

# Zsh fish-like autosuggestion.
# @see: https://github.com/zsh-users/zsh-autosuggestions
# ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE="fg=#ff00ff,bg=cyan,bold,underline"

# Plugins. 
plugins=(
  ansible
  colored-man-pages
#  common-aliases
  dotenv
  fzf
  git
  history
  httpie
  rsync
  wd 
  web-search
  zsh-autosuggestions
  zsh-syntax-highlighting
)
if [[ "$(uname -s)" == "Darwin" ]]; then
  plugins+="osx"
elif [[ "$(uname -s)" == "Linux" ]]; then
  plugins+="ubuntu"
fi

# Fallback for enviroment, and aliases.
# [[ -f "$ZSH//custom/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh" ]] && source "$ZSH//custom/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh"
[ -f "$HOME/.env" ] && source $HOME/.env || [ -f "$DOTFILES/env" ] && source $DOTFILES/env || echo "No environment file (e.g.: .env)."
[ -f "$HOME/.aliases" ] && source $HOME/.aliases || [ -f "$DOTFILES/aliases" ] && source $DOTFILES/aliases || echo "No aliases file (e.g.: .aliases)."

[ -f "$ZSH/oh-my-zsh.sh" ] && source $ZSH/oh-my-zsh.sh

if command -v pyenv > /dev/null; then eval "$(pyenv init -)"; fi
if command -v starship > /dev/null; then eval "$(starship init zsh)"; fi
if command -v exa > /dev/null; then alias ls="exa"; fi
