# Dotfiles

See more: https://dotfiles.github.io/

## Install

Clone to your home and run install:

```shell
git clone https://gitlab.com/janoka/dotfiles ~/.dotfiles
~/.dotfiles/bin/install.sh
```

## Update config files and symlinks

```shell
~/.dotfiles/bin/update.sh
```
