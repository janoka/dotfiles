# Bashrc.

# Prompt.
export PS1="\e[0;31m[\u@\h \W]\$ \e[m" 

# Dotfiles.
if [ -d "$HOME/.dotfiles" ]; then 
  export DOTFILES="$HOME/.dotfiles"
elif [ -d "/usr/local/dotfiles" ]; then
  export DOTFILES="/usr/local/dotfiles"
else 
  echo "No dotfiles installation found."
fi

# Bash git prompt.
if [ -f "$DOTFILES/bash-git-prompt/gitprompt.sh" ]; then
    GIT_PROMPT_ONLY_IN_REPO=1
    source $DOTFILES/bash-git-prompt/gitprompt.sh
fi

[[ -f "$HOME/.env" ]] && source $HOME/.env || [[ -f "$DOTFILES/env" ]] && source $DOTFILES/env || echo "No environment file (e.g.: .env)."
[[ -f "$HOME/.aliases" ]] && source $HOME/.aliases || [[ -f "$DOTFILES/aliases" ]] && source $DOTFILES/aliases || echo "No aliases file (e.g.: .aliases)."

if command -v pyenv > /dev/null; then eval "$(pyenv init -)"; fi
if command -v starship > /dev/null; then eval "$(starship init zsh)"; fi
if command -v exa > /dev/null; then alias ls="exa"; fi
