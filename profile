# Profile
export DOTFILES="$HOME/.dotfiles"
[[ -f "$DOTFILES/bashrc" ]] && source $DOTFILES/bashrc
alias php='/Applications/MAMP/bin/php/php7.3.9/bin/php -c "/Library/Application Support/appsolute/MAMP PRO/conf/php7.3.9.ini"'
alias pear='/Applications/MAMP/bin/php/php7.3.9/bin/pear'
alias pecl='/Applications/MAMP/bin/php/php7.3.9/bin/pecl'

export PATH="$HOME/.cargo/bin:$PATH"
