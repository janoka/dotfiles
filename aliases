# Aliases file for zsh, bash.
alias ans-requirements="ansible-galaxy install --role-file=requirements.yml"
alias ans-ply="ansible-playbook playbook.yml"
alias ans-tgs="ansible-playbook playbook.yml --list-tags"

# Node, npm.
alias node-global-list="npm list -g --depth=0 2>/dev/null"
alias node-local-list="npm list --depth=0 2>/dev/null"

# Tools.
alias myip="curl ifconfig.me; echo"
alias pcat="pygmentize -f terminal256 -O style=native -g"
alias dcs="php -d memory_limit=-1 $COMPOSER_HOME/vendor/bin/phpcs --standard=Drupal --extensions=php,module,inc,install,test,profile,theme,css,info,txt,md"
alias dcbf="php -d memory_limit=-1 $COMPOSER_HOME/vendor/bin/phpcbf --standard=Drupal --extensions=php,module,inc,install,test,profile,theme,css,info,txt,md"
alias psr12cs="php -d memory_limit=-1 $COMPOSER_HOME/vendor/bin/phpcs --standard=PSR12 --extensions=php,inc,css"
alias psr12cbf="php -d memory_limit=-1 $COMPOSER_HOME/vendor/bin/phpcbf --standard=PSR12 --extensions=php,inc,css"
alias mirror-website="wget -P ./ -mpck --user-agent="" -e robots=off --wait 1 -E"
alias duh="du -sh -- * | sort -h"
alias hg="history | grep"
alias gr="grep -B 5 -A 10 -n --color=auto -R * -e"
alias df="df -h"
alias rm="rm -i"
alias ll='ls --color=yes -alF'
alias l='ls --color=yes -CF'

# Shell specific aliases.
if [[ "$(echo $0)" == "bash" ]]; then
  # Aliases only for bash.
  alias ..="cd ../"
  alias ...="cd ../../"
  alias ....="cd ../../../"
  alias .....="cd ../../../../"
  alias ......="cd ../../../../../"
elif [[ "$(echo $0)" == "/bin/zsh" ]]; then
  alias -g P="2>&1| pygmentize -f terminal256"
fi

# OSX.
if [[ "$(uname -s)" == "Darwin" ]]; then
  # Dash.
  alias dsh-php="open dash://php"
  alias dsh-python="open dash://python"
  alias dsh-bash="open dash://bash"
  alias dsh="open dash://$1"
  alias chrome="open -a 'Google Chrome'"
  alias firefox="open -a 'Firefox'"
  alias safari="open -a 'Safari'"
  alias goaccess="LANG="en_US.UTF-8" && LC_ALL="en_US.UTF-8" && /usr/local/bin/goaccess"
  
  # PHP
  alias php="$MAMP_PHP_DIR/bin/php -d memory_limit=-1"
  alias php72="$MAMP_PHP72_DIR/bin/php -d memory_limit=-1"
  alias composer="$MAMP_PHP_DIR/bin/php -d memory_limit=-1 $(which composer)"

  # Open ports.
  alias ports1="netstat -nat -f inet -p tcp|grep LISTEN"
  alias ports2="lsof -Pni4 | grep LISTEN"

  # Memory, CPU.
  alias resourceinfo="top -l 1 -s 0 | head -10"
  alias psmem10="ps -emo pid,user,pcpu=CPU,pmem=MEM,command | head -11"
  alias pscpu10="ps -ero pid,user,pcpu=CPU,pmem=MEM,command | head -11"

# Linux.
elif [[ "$(uname -s)" == "Linux" ]]; then
  # Firewall.
  alias iptlist="sudo /sbin/iptables -L -n -v --line-numbers"
  alias iptlistin="sudo /sbin/iptables -L INPUT -n -v --line-numbers"
  alias iptlistout="sudo /sbin/iptables -L OUTPUT -n -v --line-numbers"
  alias iptlistfw="sudo /sbin/iptables -L FORWARD -n -v --line-numbers"
  # Ports
  alias ports1="netstat -tulnp | grep LISTEN"
  alias ports2="ss -tulnp | grep LISTEN"
  alias meminfo="free -m -l -t"
  # CPU, memory.
  alias psmem10="ps -e --sort=-pmem --format=pid,user,pcpu,pmem,command | head -11"
  alias pscpu10="ps -e --sort=-pcpu --format=pid,user,pcpu,pmem,command | head -11"
  alias cpuinfo="lscpu"
  alias bat="batcat"
fi
