#!/usr/bin/env bash
# @see: https://github.com/ohmyzsh/ohmyzsh#custom-directory

# Prerequisite.
DOTFILES="${DOTFILES:-$HOME/.dotfiles}"
ZSH="${ZSH:-$DOTFILES/oh-my-zsh}"
ZSH_AUTOSUGGESTION="${ZSH_CUSTOM:-$ZSH/custom}/plugins/zsh-autosuggestions"
ZSH_SYNTAXHIGHLIGHT="${ZSH_CUSTOM:-$ZSH/custom}/plugins/zsh-syntax-highlighting"
BASH_GIT_PROMPT="${BASH_GIT_PROMPT:-$DOTFILES/bash-git-prompt}"

# Create vim dirs.
if [[ ! -d $DOTFILES/vim/ ]]; then
  mkdir -p $DOTFILES/vim/backups $DOTFILES/vim/swaps $DOTFILES/vim/undo
fi

# Oh-my-zsh install.
if [[ ! -d $ZSH ]]; then
  git clone https://github.com/ohmyzsh/ohmyzsh.git $ZSH
  source $ZSH/tools/install.sh
fi 

# Zsh syntax highlight install.
if [[ ! -d $ZSH_SYNTAXHIGHLIGHT ]]; then
  git clone https://github.com/zsh-users/zsh-syntax-highlighting.git $ZSH_SYNTAXHIGHLIGHT
fi

# Zsh fish-like autosuggestion install.
if [[ ! -d $ZSH_AUTOSUGGESTION ]]; then
  git clone https://github.com/zsh-users/zsh-autosuggestions $ZSH_AUTOSUGGESTION
fi

# Bash git prompt.
if [[ ! -d $BASH_GIT_PROMPT ]]; then
  git clone https://github.com/magicmonty/bash-git-prompt.git $BASH_GIT_PROMPT --depth=1
fi