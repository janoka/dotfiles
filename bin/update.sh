#!/usr/bin/env bash

DOTFILES="$HOME/.dotfiles"
UPDATE_FILES=(
  "bashrc"
  "gitconfig"
  "gitignore_global"
  "my.cnf"
  "profile"
  "vimrc"
  "zshrc"
)

for ITEM in ${UPDATE_FILES[@]}; do
  if [[ -f $HOME/.$ITEM ]]; then
    while true; do
      # Read:
      #  -s: Do not echo keystrokes when read is taking input from the terminal. 
      #  -n: Stop reading after an integer number nchars characters have been read.
      #  -p: Print the string prompt, without a newline, before beginning to read.
      read -s -n 1 -p "Do you want to remove $ITEM file? [y/N]: " yesno
      echo 
      case $yesno in
        # 0=true, 1=flase.
        [Yy]* ) rm $HOME/.$ITEM; break;;
        * ) break;;
      esac
    done
  elif [[ -h $HOME/.$ITEM ]]; then
    while true; do
      read -s -n 1 -p "Do you want to remove $ITEM symbolic link? [y/N]: " yesno
      echo 
      case $yesno in
        # 0=true, 1=flase.
        [Yy]* ) rm $HOME/.$ITEM; break;;
        * ) break;;
      esac
    done
  fi

  # Create new items.
  if [[ ! -a $HOME/.$ITEM ]]; then
    while true; do
      read -s -n 1 -p "Create symbolic link (y) or a copy (N) to the '~/.$ITEM' file? [y/N]: " yesno
      echo
      case $yesno in
        # 0=true, 1=flase.
        [Nn]* ) cp $DOTFILES/$ITEM $HOME/.$ITEM; break;;
        * ) ln -s $DOTFILES/$ITEM $HOME/.$ITEM; break;;
      esac
    done
    echo
  fi
done