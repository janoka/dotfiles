#!/usr/bin/env bash
# Variables.
DOTFILES="${DOTFILES:-$HOME/.dotfiles}"
ZSH="${ZSH:-$DOTFILES/oh-my-zsh}"
FZF="${FZF:-$DOTFILES/fzf}"
BASH_GIT_PROMPT="${BASH_GIT_PROMPT:-$DOTFILES/bash-git-prompt}"

# Remove directories.
rm -rf $ZSH $FZF $BASH_GIT_PROMPT